from django.contrib import admin

from .filters import HasForeignKeyFieldListFilter
from .models import League, Round, Row, Team


class LeagueAdmin(admin.ModelAdmin):
    list_display = ('name', 'has_fd_object')
    list_filter = (('fd_object', HasForeignKeyFieldListFilter),)
    raw_id_fields = ('fd_object',)

    def has_fd_object(self, obj):
        return bool(obj.fd_object)
    has_fd_object.boolean = True
    has_fd_object.short_description = 'Linked to Football-Data'


class RoundAdmin(admin.ModelAdmin):
    list_display = ('name', 'opening_time', 'closing_time')


class RowAdmin(admin.ModelAdmin):
    list_display = (
        'round', 'index', 'match_date', 'league', 'home_team', 'away_team',
        'has_fd_object',
    )
    list_filter = (('fd_object', HasForeignKeyFieldListFilter),)

    def match_date(self, obj):
        return obj.match_start.date()

    def has_fd_object(self, obj):
        return bool(obj.fd_object)
    has_fd_object.boolean = True
    has_fd_object.short_description = 'Linked to Football-Data'


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'has_fd_object')
    list_filter = (('fd_object', HasForeignKeyFieldListFilter),)
    raw_id_fields = ('fd_object',)

    def has_fd_object(self, obj):
        return bool(obj.fd_object)
    has_fd_object.boolean = True
    has_fd_object.short_description = 'Linked to Football-Data'


admin.site.register(League, LeagueAdmin)
admin.site.register(Round, RoundAdmin)
admin.site.register(Row, RowAdmin)
admin.site.register(Team, TeamAdmin)
