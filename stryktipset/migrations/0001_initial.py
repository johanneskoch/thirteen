# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('footballdata', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('svs_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=100)),
                ('fd_object', models.OneToOneField(null=True, blank=True, to='footballdata.LeagueDivision')),
            ],
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('svs_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=6)),
                ('opening_time', models.DateTimeField()),
                ('closing_time', models.DateTimeField()),
            ],
            options={
                'ordering': ('-opening_time',),
            },
        ),
        migrations.CreateModel(
            name='Row',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', models.IntegerField()),
                ('match_start', models.DateTimeField()),
            ],
            options={
                'ordering': ('-round__name', 'index'),
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('svs_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=100)),
                ('name_short', models.CharField(max_length=100)),
                ('fd_object', models.OneToOneField(null=True, blank=True, to='footballdata.Team')),
            ],
        ),
        migrations.AddField(
            model_name='row',
            name='away_team',
            field=models.ForeignKey(related_name='away_matches', to='stryktipset.Team'),
        ),
        migrations.AddField(
            model_name='row',
            name='fd_object',
            field=models.OneToOneField(null=True, blank=True, to='footballdata.Match'),
        ),
        migrations.AddField(
            model_name='row',
            name='home_team',
            field=models.ForeignKey(related_name='home_matches', to='stryktipset.Team'),
        ),
        migrations.AddField(
            model_name='row',
            name='league',
            field=models.ForeignKey(to='stryktipset.League'),
        ),
        migrations.AddField(
            model_name='row',
            name='round',
            field=models.ForeignKey(to='stryktipset.Round'),
        ),
        migrations.AlterUniqueTogether(
            name='row',
            unique_together=set([('round', 'index')]),
        ),
    ]
