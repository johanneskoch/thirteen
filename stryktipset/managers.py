from django.db import models


class LeagueManager(models.Manager):
    """ """
    def from_element(self, element):
        return self.get_or_create(
            svs_id=element.get('league_id'),
            defaults={
                'name': element.get('league_name'),
            },
        )


class TeamManager(models.Manager):
    """ """
    def from_element(self, element):
        return self.get_or_create(
            svs_id=element.get('id'),
            defaults={
                'name': element.get('name'),
                'name_short': element.get('name_short'),
            },
        )


class RoundManager(models.Manager):
    """ """
    def from_element(self, element):
        return self.get_or_create(
            svs_id=element.get('round_id'),
            defaults={
                'name': element.get('round_name'),
                'opening_time': element.get('opening_time'),
                'closing_time': element.get('closing_time'),
            },
        )
