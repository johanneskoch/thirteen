from datetime import datetime

from django.views.generic import DetailView

from .models import Round


class RoundDetailView(DetailView):
    """ """
    model = Round
    slug_field = 'name'
    slug_url_kwarg = 'name'

    def get_context_data(self, **kwargs):
        context = super(RoundDetailView, self).get_context_data(**kwargs)

        try:
            context['prev_round'] = self.object.get_previous_by_opening_time()
        except Round.DoesNotExist:
            pass

        try:
            context['next_round'] = self.object.get_next_by_opening_time()
        except Round.DoesNotExist:
            pass

        return context


class CurrentRoundDetailView(RoundDetailView):
    """ """
    def get_object(queryset=None):
        return Round.objects.exclude(opening_time__gt=datetime.now()).latest()
