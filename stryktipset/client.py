from xml.etree.ElementTree import Element, SubElement, fromstring, tostring

import requests


class StryktipsetClient(object):
    """ """
    api_endpoint = 'https://svenskaspel.se/isapi/mhandler.dll'

    def _build_payload(self, endpoint, data):
        """ """
        root = Element('svsxml', {'channel': 'INTERNET'})
        endpoint = SubElement(root, endpoint, {'ver': '1.0'})

        for key, value in data.items():
            SubElement(endpoint, key).text = str(value)

        return tostring(root)

    def _make_api_call(self, endpoint, data):
        """ """
        data = data.copy()
        data.update(product_id=1)
        response = requests.post(
            self.api_endpoint, self._build_payload(endpoint, data))

        response_xml = fromstring(response.content)
        response_data = list(response_xml.find('{}_response'.format(endpoint)))
        result = response_data.pop(0)

        return response_data

    def get_rounds(self):
        """ """
        return self._make_api_call('get_rounds', {'round_id': 0})

    def get_round_info(self, round_id):
        """ """
        return self._make_api_call('get_round_info', {'round_id': round_id})[0]
