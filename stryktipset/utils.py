from .client import StryktipsetClient
from .models import League, Round, Row, Team


def import_round(round_id):
    """ """
    client = StryktipsetClient()
    round_info = client.get_round_info(round_id)
    round, created = Round.objects.from_element(round_info)

    if created:
        for row in round_info.iterfind('coupon/row'):
            league = League.objects.from_element(row.find('match'))[0]
            home_team = Team.objects.from_element(
                row.find('match/home_team'))[0]
            away_team = Team.objects.from_element(
                row.find('match/away_team'))[0]

            Row.objects.get_or_create(
                round=round,
                index=row.get('id'),
                defaults={
                    'match_start': row.find('match').get('start'),
                    'league': league,
                    'home_team': home_team,
                    'away_team': away_team,
                },
            )


def update():
    """ """
    client = StryktipsetClient()
    for round in client.get_rounds():
        import_round(round.get('round_id'))


def match():
    """ """
    for team in Team.objects.filter(fd_object__isnull=True):
        team.match()

    for row in Row.objects.filter(fd_object__isnull=True):
        row.match()
