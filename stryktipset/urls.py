from django.conf.urls import url

from .views import RoundDetailView, CurrentRoundDetailView


urlpatterns = [
    url(r'^$', CurrentRoundDetailView.as_view()),
    url(
        r'^(?P<name>[0-9]{6})/$',
        RoundDetailView.as_view(),
        name='stryktipset_round_detail',
    ),
]
