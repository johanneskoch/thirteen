from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q

from .managers import LeagueManager, RoundManager, TeamManager
from footballdata.models import Match, Team as FDTeam


class League(models.Model):
    """ """
    fd_object = models.OneToOneField(
        'footballdata.LeagueDivision',
        blank=True,
        null=True,
        verbose_name='link to Football-Data',
    )

    svs_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=100)

    objects = LeagueManager()

    def __unicode__(self):
        return self.name

    def match(self):
        """ """
        raise NotImplementedError  # For God's sake, there's not that many


class Team(models.Model):
    """ """
    fd_object = models.OneToOneField(
        'footballdata.Team',
        blank=True,
        null=True,
        verbose_name='link to Football-Data',
    )

    svs_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=100)
    name_short = models.CharField(max_length=100)

    objects = TeamManager()

    def __unicode__(self):
        return self.name

    def match(self):
        """ """
        try:
            match = FDTeam.objects.get(
                Q(name__istartswith=self.name) |
                Q(name__istartswith=self.name_short)
            )
            self.fd_object = match
            self.save()
            return match
        except FDTeam.DoesNotExist:
            return None
        except FDTeam.MultipleObjectsReturned:
            return None


class Round(models.Model):
    """ """
    svs_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=6)
    opening_time = models.DateTimeField()
    closing_time = models.DateTimeField()

    objects = RoundManager()

    class Meta:
        ordering = ('-opening_time',)
        get_latest_by = 'opening_time'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('stryktipset_round_detail', args=[self.name])


class Row(models.Model):
    """ """
    fd_object = models.OneToOneField(
        'footballdata.Match',
        blank=True,
        null=True,
        verbose_name='link to Football-Data',
    )

    round = models.ForeignKey('Round', related_name='rows')
    index = models.IntegerField()
    match_start = models.DateTimeField()
    league = models.ForeignKey('League')
    home_team = models.ForeignKey('Team', related_name='home_matches')
    away_team = models.ForeignKey('Team', related_name='away_matches')

    class Meta:
        unique_together = ('round', 'index')
        ordering = ('-round__name', 'index')

    def __unicode__(self):
        return '{0.round} ({0.index})'.format(self)

    def match(self):
        """ """
        try:
            match = Match.objects.get(
                match_date=self.match_start.date(),
                home_team=self.home_team.fd_object,
                away_team=self.away_team.fd_object
            )
            self.fd_object = match
            self.save()
            return match
        except Match.DoesNotExist:
            return None
        except Match.MultipleObjectsReturned:
            return None
