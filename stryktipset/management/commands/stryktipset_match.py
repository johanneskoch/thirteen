from django.core.management.base import BaseCommand

from stryktipset.utils import match


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        match()
