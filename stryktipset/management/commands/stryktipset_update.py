from django.core.management.base import BaseCommand

from stryktipset.utils import update


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        update()
