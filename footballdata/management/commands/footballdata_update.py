from django.core.management.base import BaseCommand

from footballdata.utils import update


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('csv_url', type=str)
        parser.add_argument('season_name', type=str, nargs='?')

    def handle(self, *args, **options):
        update(options['csv_url'], options['season_name'])
