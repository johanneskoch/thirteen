# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LeagueDivision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('abbreviation', models.CharField(unique=True, max_length=3)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('match_date', models.DateField()),
                ('full_time_home_team_goals', models.IntegerField()),
                ('full_time_away_team_goals', models.IntegerField()),
                ('full_time_result', models.CharField(max_length=1, choices=[(b'H', b'Home Win'), (b'D', b'Draw'), (b'A', b'Away Win')])),
                ('half_time_home_team_goals', models.IntegerField()),
                ('half_time_away_team_goals', models.IntegerField()),
                ('half_time_result', models.CharField(max_length=1, choices=[(b'H', b'Home Win'), (b'D', b'Draw'), (b'A', b'Away Win')])),
                ('crowd_attendance', models.IntegerField(null=True, blank=True)),
                ('home_team_shots', models.IntegerField()),
                ('away_team_shots', models.IntegerField()),
                ('home_team_shots_on_target', models.IntegerField()),
                ('away_team_shots_on_target', models.IntegerField()),
                ('home_team_hit_woodwork', models.IntegerField(null=True, blank=True)),
                ('away_team_hit_woodwork', models.IntegerField(null=True, blank=True)),
                ('home_team_corners', models.IntegerField()),
                ('away_team_corners', models.IntegerField()),
                ('home_team_fouls_committed', models.IntegerField()),
                ('away_team_fouls_committed', models.IntegerField()),
                ('home_team_offsides', models.IntegerField(null=True, blank=True)),
                ('away_team_offsides', models.IntegerField(null=True, blank=True)),
                ('home_team_yellow_cards', models.IntegerField()),
                ('away_team_yellow_cards', models.IntegerField()),
                ('home_team_red_cards', models.IntegerField()),
                ('away_team_red_cards', models.IntegerField()),
            ],
            options={
                'ordering': ('-match_date',),
                'verbose_name_plural': 'matches',
            },
        ),
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'ordering': ('-name',),
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='away_team',
            field=models.ForeignKey(related_name='away_matches', to='footballdata.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ForeignKey(related_name='home_matches', to='footballdata.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='league_division',
            field=models.ForeignKey(to='footballdata.LeagueDivision'),
        ),
        migrations.AddField(
            model_name='match',
            name='season',
            field=models.ForeignKey(blank=True, to='footballdata.Season', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='match',
            unique_together=set([('match_date', 'home_team')]),
        ),
    ]
