import csv
import datetime

import requests

from .models import LeagueDivision, Match, Season, Team


MAPPING = {
    'Div': 'league_division',
    'Date': 'match_date',
    'HomeTeam': 'home_team',
    'AwayTeam': 'away_team',
    'FTHG': 'full_time_home_team_goals',
    'FTAG': 'full_time_away_team_goals',
    'FTR': 'full_time_result',
    'HTHG': 'half_time_home_team_goals',
    'HTAG': 'half_time_away_team_goals',
    'HTR': 'half_time_result',
    'Attendance': 'crowd_attendance',
    'HS': 'home_team_shots',
    'AS': 'away_team_shots',
    'HST': 'home_team_shots_on_target',
    'AST': 'away_team_shots_on_target',
    'HHW': 'home_team_hit_woodwork',
    'AHW': 'away_team_hit_woodwork',
    'HC': 'home_team_corners',
    'AC': 'away_team_corners',
    'HF': 'home_team_fouls_committed',
    'AF': 'away_team_fouls_committed',
    'HO': 'home_team_offsides',
    'AO': 'away_team_offsides',
    'HY': 'home_team_yellow_cards',
    'AY': 'away_team_yellow_cards',
    'HR': 'home_team_red_cards',
    'AR': 'away_team_red_cards',
}


def update(csv_url, season_name=None):
    """ """
    if season_name:
        season = Season.objects.get_or_create(name=season_name)[0]
    else:
        season = None

    response = requests.get(csv_url)
    reader = csv.DictReader(response.iter_lines())
    for row in reader:
        data = {MAPPING[k]: v for k, v in row.items() if k in MAPPING}
        data.update(
            league_division=LeagueDivision.objects.get_or_create(
                abbreviation=data['league_division'])[0],
            season=season,
            match_date=datetime.datetime.strptime(
                data['match_date'], '%d/%m/%y').date(),
            home_team=Team.objects.get_or_create(name=data['home_team'])[0],
            away_team=Team.objects.get_or_create(name=data['away_team'])[0],
        )
        unique_data = {
            'match_date': data.pop('match_date'),
            'home_team': data.pop('home_team'),
        }
        Match.objects.get_or_create(defaults=data, **unique_data)
