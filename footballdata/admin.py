from django.contrib import admin

from .models import LeagueDivision, Match, Season, Team


class LeagueDivisionAdmin(admin.ModelAdmin):
    list_display = ('abbreviation', 'name')


class MatchAdmin(admin.ModelAdmin):
    list_display = (
        'match_date', 'league_division', 'home_team', 'away_team',
        'full_time_result',
    )


class TeamAdmin(admin.ModelAdmin):
    search_fields = ('name',)


admin.site.register(LeagueDivision, LeagueDivisionAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(Season)
admin.site.register(Team, TeamAdmin)
