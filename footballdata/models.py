from django.db import models


class LeagueDivision(models.Model):
    """ """
    abbreviation = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return '{0.abbreviation} ({0.name})'.format(self)


class Season(models.Model):
    """ """
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ('-name',)

    def __unicode__(self):
        return self.name


class Team(models.Model):
    """ """
    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class Match(models.Model):
    """ """
    HOME_WIN = 'H'
    DRAW = 'D'
    AWAY_WIN = 'A'
    RESULT_CHOICES = (
        (HOME_WIN, 'Home Win'),
        (DRAW, 'Draw'),
        (AWAY_WIN, 'Away Win'),
    )

    league_division = models.ForeignKey('LeagueDivision')
    season = models.ForeignKey('Season', blank=True, null=True)
    match_date = models.DateField()
    home_team = models.ForeignKey('Team', related_name='home_matches')
    away_team = models.ForeignKey('Team', related_name='away_matches')

    full_time_home_team_goals = models.IntegerField()
    full_time_away_team_goals = models.IntegerField()
    full_time_result = models.CharField(max_length=1, choices=RESULT_CHOICES)
    half_time_home_team_goals = models.IntegerField()
    half_time_away_team_goals = models.IntegerField()
    half_time_result = models.CharField(max_length=1, choices=RESULT_CHOICES)

    crowd_attendance = models.IntegerField(blank=True, null=True)

    home_team_shots = models.IntegerField()
    away_team_shots = models.IntegerField()
    home_team_shots_on_target = models.IntegerField()
    away_team_shots_on_target = models.IntegerField()
    home_team_hit_woodwork = models.IntegerField(blank=True, null=True)
    away_team_hit_woodwork = models.IntegerField(blank=True, null=True)
    home_team_corners = models.IntegerField()
    away_team_corners = models.IntegerField()

    home_team_fouls_committed = models.IntegerField()
    away_team_fouls_committed = models.IntegerField()
    home_team_offsides = models.IntegerField(blank=True, null=True)
    away_team_offsides = models.IntegerField(blank=True, null=True)
    home_team_yellow_cards = models.IntegerField()
    away_team_yellow_cards = models.IntegerField()
    home_team_red_cards = models.IntegerField()
    away_team_red_cards = models.IntegerField()

    class Meta:
        unique_together = ('match_date', 'home_team')  # ... right?
        ordering = ('-match_date',)
        verbose_name_plural = 'matches'

    def __unicode__(self):
        return self.match_date.isoformat()

    def result(self):
        """ """
        return dict(self.RESULT_CHOICES)[self.full_time_result]
