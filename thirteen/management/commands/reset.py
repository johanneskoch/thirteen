from django.contrib.auth import get_user_model
from django.core import management
from django.core.management.base import BaseCommand
from django.db import DEFAULT_DB_ALIAS
from django.db.models import Min

from footballdata.utils import update as footballdata_update
from stryktipset.models import Round
from stryktipset.utils import import_round, update as stryktipset_update, match


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        # Migrate
        management.call_command('migrate')

        # Create superuser
        self.stdout.write(
            self.style.MIGRATE_HEADING('Creating superuser...'), ending='')
        self.stdout.flush()
        (
            get_user_model()
            ._default_manager
            .db_manager(DEFAULT_DB_ALIAS)
            .create_superuser(
                username='Admin', email='me@johannesko.ch', password='123')
        )
        self.stdout.write(self.style.MIGRATE_SUCCESS(' DONE'))

        # Football-Data import
        management.call_command('loaddata', 'leaguedivisions')

        self.stdout.write(self.style.MIGRATE_HEADING(
            'Importing from football-data.co.uk:'))
        fd_url = 'http://www.football-data.co.uk/mmz4281/{0}/{1}.csv'
        fd_seasons = [
            ('1415', 'Season 2014/2015'),
            ('1314', 'Season 2013/2014'),
            ('1213', 'Season 2012/2013'),
        ]
        fd_league_divisons = ['E0', 'E1', 'E2', 'E3']
        for season in fd_seasons:
            for league_divison in fd_league_divisons:
                self.stdout.write(
                    '  Importing {0}, Division {1}...'.format(
                        season[1], league_divison),
                    ending='',
                )
                self.stdout.flush()
                footballdata_update(
                    fd_url.format(season[0], league_divison), season[1])
                self.stdout.write(self.style.MIGRATE_SUCCESS(' OK'))

        # Stryktipset import
        self.stdout.write(
            self.style.MIGRATE_HEADING(
                'Importing current rounds from svenskaspel.se...'),
            ending='',
        )
        self.stdout.flush()
        stryktipset_update()
        self.stdout.write(self.style.MIGRATE_SUCCESS(' DONE'))

        self.stdout.write(self.style.MIGRATE_HEADING(
            'Importing historical rounds:'))
        st_current = Round.objects.aggregate(Min('svs_id'))['svs_id__min']
        st_weeks = 52
        for round_id in range(st_current - st_weeks, st_current):
            self.stdout.write(
                '  Importing round {}...'.format(round_id), ending='')
            self.stdout.flush()
            import_round(round_id)
            self.stdout.write(self.style.MIGRATE_SUCCESS(' OK'))

        match()
